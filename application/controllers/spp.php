<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Spp extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Spp_model');
	}

	public function index()

	{
		$data['query'] = $this->Spp_model->read();
		$this->load->view('spp/index', $data);
	}

	public function add()
	{
		$this->load->view('spp/add');
	}

	public function insert()
	{

		$tahun = $this->input->post('tahun');
		$nominal = $this->input->post('nominal');

		echo $tahun .  " - " . $nominal;

		$data['query'] = $this->Spp_model->create();

		redirect('spp');
	}


	public function delete($id_spp)
	{

		$this->spp_model->delete($id_spp);
		redirect('spp');
	}
}
