<?php
defined('BASEPATH') or exit('No direct script access allowed');

class petugas extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('petugas_model');
    }

    public function index()

    {
        $data['query'] = $this->petugas_model->read();
        $this->load->view('petugas/index', $data);
    }

    public function add()
    {
        $this->load->view('petugas/add');
    }

    public function insert()
    {

        $username = $this->input->post('username');
        $password = $this->input->post('password');
        $nama_petugas = $this->input->post('nama_petugas');
        $level = $this->input->post('level');


        echo $username .  " - " . $password . $nama_petugas .  " - " . $level;

        $data['query'] = $this->petugas_model->create();

        redirect('petugas');
    }


    public function delete($id_petugas)
    {
        $this->petugas_model->delete($id_petugas);
        redirect('petugas');
    }
}
