<?php
defined('BASEPATH') or exit('No direct script access allowed');

class pembayaran extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('pembayaran_model');
    }

    public function index()

    {
        $data['query'] = $this->pembayaran_model->read();
        $this->load->view('pembayaran/index', $data);
    }

    public function add()
    {
        $this->load->view('pembayaran/add');
    }

    public function insert()
    {

        $id_pembayran = $this->input->post('id_pembayaran');
        $id_petugas = $this->input->post('id_petugas');
        $tgl_bayar = $this->input->post('tgl_bayar');
        $bulan_bayar = $this->input->post('bulan_bayar');
        $tahun_bayar = $this->input->post('tahun_pembayaran');
        $jumlah_bayar = $this->input->post('jumlah_bayar');

        echo $id_pembayran .  " - " . $id_petugas . " - " . $tgl_bayar .  " - " . $bulan_bayar . " - " . $tahun_bayar . " - " . $jumlah_bayar;

        $data['query'] = $this->pembayaran_model->create();

        redirect('spp');
    }


    public function delete($id_pembayaran)
    {

        $this->spp_model->delete($id_pembayaran);
        redirect('pembayaran');
    }
}
