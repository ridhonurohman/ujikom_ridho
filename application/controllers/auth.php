<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends CI_Controller 
{ 
    public function __construct()
    {
       parent::__construct();
       $this->load->library('form_validation');

    }
    public function index()
    {
        $this->load->view('templates/auth_header');
        $this->load->view('auth/login');
        $this->load->view('templates/auth_footer');
        
    }
 
    public function registration()
    {
     
     $this->form_validation->set_rules('name', 'name', 'required|trim');
     $this->form_validation->set_rules('email', 'email','required|trim|valid_email|<is_unique user.email=""></is_unique>');
    $this->form_validation->set_rules('password1', 'password', 'required|trim| min_lenght[3]|matches[paassword2]');
   
    $this->form_validation->set_rules('password2', 'password', 'required|trim|matches[paassword2]');

        if( $this->form_validation->run() == false) {
         $data['title'] ='login page';
         $this->load->view('templates/auth_header',$data);
         $this->load->view('auth/registration');
         $this->load->view('templates/auth_footer');
     } else {
    
        $data =[
            
            'name'=> $this->input->post('name'),
            'email'=> $this->input->post('email'),
            'image'=>'default.jpg',
            'password'=> password_hash($this->input->post('passowrd'),PASSWORD_DEFAULT),
            'role_id'=> 2,
            'is_active'=> 1,
            'date_created'=> time()

        ];
        
        $this->db->insert('user, $sata');
     
     }
    }
}
      
                                                                                                                                                                           

  