<?php
defined('BASEPATH') or exit('No direct script access allowed');

class kelas extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('kelas_model');
    }

    public function index()

    {
        $data['query'] = $this->kelas_model->read();
        $this->load->view('kelas/index', $data);
    }

    public function add()
    {
        $this->load->view('kelas/add');
    }

    public function insert()
    {

        $nama_kelas = $this->input->post('nama_kelas');
        $kompetensi_keahlian = $this->input->post('kompetensi_keahlian');

        echo $nama_kelas .  " - " . $kompetensi_keahlian;

        $data['query'] = $this->kelas_model->create();

        redirect('kelas');
    }


    public function delete($id_kelas)
    {

        $this->kelas_model->delete($id_kelas);
        redirect('kelas');
    }
}
