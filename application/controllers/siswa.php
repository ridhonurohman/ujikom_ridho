<?php
defined('BASEPATH') or exit('No direct script access allowed');

class pembayaran extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('siswa_model');
    }

    public function index()

    {
        $data['query'] = $this->siswa_model->read();
        $this->load->view('siswa/index', $data);
    }

    public function edit($nisn)
    {
        $data['query'] = $this->siswa_model->read_by_id($nisn);
        $this->load->view('siswa/index', $data);
    }

    public function add()
    {
        $this->load->view('siswa/add');
    }

    public function insert()
    {

        $nisn = $this->input->post('nisn');
        $nis = $this->input->post('nis');
        $nama = $this->input->post('nama');
        $id_kelas = $this->input->post('id_kelas');
        $alamat = $this->input->post('alamat');
        $no_telp = $this->input->post('no_telp');
        $id_spp = $this->input->post('id_Spp');

        echo $nisn .  " - " . $nis . " - " . $nama .  " - " . $id_kelas . " - " . $alamat  . " - " . $no_telp . " - " . $id_spp;

        $data['query'] = $this->siswa_model->create();

        redirect('siswa');
    }


    public function delete($nisn)
    {

        $this->spp_model->delete($nisn);
        redirect('siswa');
    }
}
