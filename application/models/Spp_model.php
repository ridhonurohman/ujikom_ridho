<?php

class Spp_model extends CI_Model
{
    public function create()
    {
        $data = array(
            'tahun' => $this->input->post('tahun'),
            'nominal' => $this->input->post('nominal')


        );
        $this->db->insert('spp', $data);
    }
    public function read()
    {
        $query = $this->db->get('spp');
        return $query;
    }
    public function update()
    {
        #code...
    }
    public function delete($id_spp)
    {
        $this->db->delete('spp', array('id_spp' => $id_spp));  // Produces: // DELETE FROM mytable  // WHERE id = $id
    }
}
